import os
import time
import random

from minio import Minio
from flask import Flask, jsonify, request
# import db
import redis
import requests
import time
import os
import pickle
import json

import tensorflow as tf
import tensorflow_hub as hub
import faiss
import numpy as np


def get_generation_data():
    # данные mounted local directory со своим набором кластеров для каждой ноды (параметр constraint помогает их не смешивать) 
    clusters_raw = json.load(open('/data/questions_node.json', 'rt'))
    return clusters_raw

clusters_raw = get_generation_data()

cluster_to_faiss = {}
cluster_to_id_to_question = {}
for cluster in clusters_raw.keys():
    cluster_raw_questions = clusters_raw[cluster]
    cluster_data_embeddings = np.random.rand(100, 512).astype(np.float32)

    cluster_faiss_index = faiss.IndexFlatL2(cluster_data_embeddings.shape[1])
    cluster_faiss_index = faiss.IndexIDMap(cluster_faiss_index)
    cluster_faiss_index.train(cluster_data_embeddings)
    cluster_faiss_index.add_with_ids(cluster_data_embeddings, np.arange(len(cluster_data_embeddings)))
    cluster_to_faiss[cluster] = cluster_faiss_index
    cluster_to_id_to_question[cluster] = {
        i: question for i, question in enumerate(cluster_raw_questions)
    }


app = Flask(__name__)
@app.route('/get_nearest_document', methods=['GET'])
def get_nearest_document():
    cluster = request.args.get('cluster')
    question = request.args.get('question')

    nearest_ids = cluster_to_faiss[cluster].search(
        np.random.rand(1, 512).astype(np.float32), 10)[1][0]
    nearest_id = nearest_ids[0]
    return jsonify({'nearest_question': cluster_to_id_to_question[cluster][nearest_id]})

@app.route('/healthz', methods=['GET'])
def health():
    return jsonify({'status': 'ok'}), 200

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5020)
