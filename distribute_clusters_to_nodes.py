from minio import Minio
import json
import pickle
import numpy as np
import os
import paramiko

NUMBER_OF_NODES = int(os.getenv('NUMBER_OF_NODES'))
DATA_GENERATION_NUMBER = os.getenv('DATA_GENERATION_NUMBER')
NODES_DATA = eval(os.getenv('NODES_DATA'))
MINIO_ACCESS_KEY = os.getenv('MINIO_ACCESS_KEY')
MINIO_SECRET_KEY = os.getenv('MINIO_SECRET_KEY')
MINIO_URL = os.getenv('MINIO_URL')
bucket_name = f'data-generation-{DATA_GENERATION_NUMBER}'

s3_client = Minio(
    MINIO_URL,
    access_key=MINIO_ACCESS_KEY,
    secret_key=MINIO_SECRET_KEY,
    secure=False
)
clusters_centers_name = f'clusters_centers_use_dg{DATA_GENERATION_NUMBER}.pkl'
clusters_use_data_name = f'clusters_use_dg{DATA_GENERATION_NUMBER}.json'
node_to_clusters_name = f'node_to_clusters_dg{DATA_GENERATION_NUMBER}.json'

response = s3_client.fget_object(bucket_name, clusters_centers_name, clusters_centers_name)
response = s3_client.fget_object(bucket_name, clusters_use_data_name, clusters_use_data_name)

clusters_centers = pickle.load(open(clusters_centers_name, 'rb'))
clusters_use_data = json.load(open(clusters_use_data_name, 'rt'))
clusters_raw_keys = clusters_use_data.keys()

node_to_clusters = {i: [] for i in range(NUMBER_OF_NODES)}

for cluster_number, cluster in enumerate(clusters_centers):
    if cluster not in clusters_raw_keys:
        continue
    node = cluster_number % NUMBER_OF_NODES
    node_to_clusters[node].append(cluster)


json.dump(node_to_clusters, open(node_to_clusters_name, 'wt'))
node_to_clusters

s3_client.fput_object(
    bucket_name, node_to_clusters_name, node_to_clusters_name,
)

def send_files_to_servers():
    for server, node_number in NODES_DATA:
        key = paramiko.RSAKey.from_private_key_file('/root/.ssh/id_rsa')
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_client.connect(
            hostname=server,
            username="root",
            pkey=key
        )

        path = f"/final/data/{DATA_GENERATION_NUMBER}"
        ssh_client.exec_command(f"mkdir -p {path}")
        ssh_client.exec_command(f"rm -r {path}/*")

        node_clusters = node_to_clusters[node_number]
        questions_node = {cluster: value for cluster, value in clusters_use_data.items() if cluster in node_clusters}
        json.dump(questions_node, open('questions_node.json', 'wt'))
        
        ftp_client = ssh_client.open_sftp()
        ftp_client.put("questions_node.json", f"{path}/questions_node.json")
        ftp_client.close()
        ssh_client.close()

send_files_to_servers()


