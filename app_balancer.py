import os
import time
import random

from minio import Minio
from flask import Flask, jsonify, request
# import db
import redis
import requests
import time
import os
import pickle
import json

import tensorflow as tf
import tensorflow_hub as hub
import faiss
import numpy as np



def get_generation_data():
    NUMBER_OF_NODES = int(os.getenv('NUMBER_OF_NODES'))
    NODE_NUMBER = os.getenv('NODE_NUMBER')
    DATA_GENERATION_NUMBER = os.getenv('DATA_GENERATION_NUMBER')
    
    MINIO_ACCESS_KEY = os.getenv('MINIO_ACCESS_KEY')
    MINIO_SECRET_KEY = os.getenv('MINIO_SECRET_KEY')
    MINIO_URL = os.getenv('MINIO_URL')
    bucket_name = f'data-generation-{DATA_GENERATION_NUMBER}'


    s3_client = Minio(
        MINIO_URL,
        access_key=MINIO_ACCESS_KEY,
        secret_key=MINIO_SECRET_KEY,
        secure=False
    )
    node_to_clusters_name = f'node_to_clusters_dg{DATA_GENERATION_NUMBER}.json'
    clusters_centers_name = f'clusters_centers_use_dg{DATA_GENERATION_NUMBER}.pkl'
    response = s3_client.fget_object(bucket_name, node_to_clusters_name, node_to_clusters_name)
    response = s3_client.fget_object(bucket_name, clusters_centers_name, clusters_centers_name)

    node_to_clusters = json.load(open(node_to_clusters_name, 'rt'))
    cluster_to_node = {}
    for node, clusters in node_to_clusters.items():
        for cluster in clusters:
            cluster_to_node[cluster] = node
    clusters_centers = pickle.load(open(clusters_centers_name, 'rb'))

    clusters_centers = {cluster: center for cluster, center in clusters_centers.items() if cluster in cluster_to_node.keys()}
    return cluster_to_node, clusters_centers

def find_nearest_cluster(question_embeddings, clusters_centers):
    min_distance = None
    nearest_cluster = None
    for cluster, center in clusters_centers.items():
        distance = np.linalg.norm(center - question_embeddings)
        if min_distance is None or distance < min_distance:
            min_distance = distance
            nearest_cluster = cluster
    return nearest_cluster

def get_node_to_port():
    NUMBER_OF_NODES = int(os.getenv('NUMBER_OF_NODES'))
    node_to_port = {}
    for node_number in range(NUMBER_OF_NODES):
        node_to_port[str(node_number)] = 5020 + node_number
    return node_to_port

cluster_to_node, clusters_centers = get_generation_data()
node_to_port = get_node_to_port()


cluster_to_node, clusters_centers = get_generation_data()

model_use = hub.load('https://tfhub.dev/google/universal-sentence-encoder-qa/3')


app = Flask(__name__)
@app.route('/get_nearest_document', methods=['GET'])
def get_nearest_document():
    question = request.args.get('question')

    question_embeddings = model_use.signatures['question_encoder'](
        tf.constant([question]))['outputs'].numpy().ravel()
    nearest_cluster = find_nearest_cluster(question_embeddings, clusters_centers)
    port = node_to_port[cluster_to_node[nearest_cluster]]

    result = requests.get(
        url=f'http://95.216.143.2:{port}/get_nearest_document',
        params={
            "cluster": nearest_cluster,
            "question": question
        }
    )
    return result.text

@app.route('/healthz', methods=['GET'])
def health():
    return jsonify({'status': 'ok'}), 200


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5019)

